#!/bin/bash

firstParameter=$1
frontEnvironment="--front"
backEnvironment="--back"
fullEnvironment="--full"

if [[ $firstParameter == "--help" || $firstParameter == "" ]]; then
    echo "Syntax: bash run.sh [--front | --back | --full]"
    echo
    echo "Options:"
    echo
    echo "--front    Runs only frontend"
    echo "--back     Runs only backend"
    echo "--full     Runs frontend and backend"
    echo
    exit;
fi

if [ $firstParameter != "" ]; then
    
    echo
    echo '----- Stopping Images'
    echo
    
    docker stop $(docker ps -qa)
    
    cd ../../laradock
    
    if [[ $firstParameter == $backEnvironment || $firstParameter == $fullEnvironment ]]; then
        echo
        echo '----- Backend'
        echo
        
        docker-compose up -d nginx mariadb php-worker redis minio #sonarqube
    fi
    
    if [[ $firstParameter == $frontEnvironment || $firstParameter == $fullEnvironment ]]; then
        
        echo
        echo '----- Stopping all nodes'
        echo
        
        killall node
        
        echo
        echo '----- Frontend'
        echo
        
        (cd ../landing-app && npm run serve) & (cd ../dashboard-app && npm run serve)
    fi
fi