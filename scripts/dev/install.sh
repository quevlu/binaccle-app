#!/bin/bash

echo
echo '----- Stopping Images'
echo

docker stop $(docker ps -qa)

echo
echo '----- Adding hosts'
echo

sudo -- sh -c "grep -qxF '127.0.0.1 binaccle.org' /etc/hosts || echo '127.0.0.1 binaccle.org' >> /etc/hosts"
sudo -- sh -c "grep -qxF '127.0.0.1 dashboard.binaccle.org' /etc/hosts || echo '127.0.0.1 dashboard.binaccle.org' >> /etc/hosts"
sudo -- sh -c "grep -qxF '127.0.0.1 api.binaccle.org' /etc/hosts || echo '127.0.0.1 api.binaccle.org' >> /etc/hosts"
sudo -- sh -c "grep -qxF '127.0.0.1 sonar.binaccle.org' /etc/hosts || echo '127.0.0.1 sonar.binaccle.org' >> /etc/hosts"
sudo -- sh -c "grep -qxF '127.0.0.1 files.binaccle.org' /etc/hosts || echo '127.0.0.1 files.binaccle.org' >> /etc/hosts"

echo
echo '----- Pulling Api'
echo

cd ../../laradock

git checkout .

git checkout master && git pull

cd ..

echo
echo '----- Configurating environment'
echo

# Copy .env.docker
cp scripts/dev/conf/.env.example laradock/.env

# Copy create database script
cp scripts/shared/createdb.sql laradock/mariadb/docker-entrypoint-initdb.d

# Copy nginx conf
cp scripts/dev/conf/default.conf laradock/nginx/sites/

# Copy supervisord conf
cp scripts/shared/laravel-worker.conf laradock/php-worker/supervisord.d/

echo
echo '----- Pulling Api'
echo

cd dashboard-api
git checkout master && git pull
cp .env.example .env

cd ..

echo
echo '----- Pulling Landing'
echo

cd landing-app
git checkout master && git pull

cd ..

echo
echo '----- Pulling Dashboard'
echo

cd dashboard-app
git checkout master && git pull

cd ..

echo
echo '----- Starting Docker'
echo

cd laradock

docker-compose up -d --build nginx mariadb php-worker redis minio #sonarqube

sleep 5s

echo
echo '----- Creating Database'
echo

docker-compose exec mariadb sh -c "mysql -u root -proot < /docker-entrypoint-initdb.d/createdb.sql"

echo
echo '----- Installing Api'
echo

docker-compose exec --user=laradock workspace sh -c "cd dashboard-api && composer install && php artisan cleanup-and-start"

echo
echo '----- Installing Frontends'
echo

cd ../landing-app

npm i

cd ../dashboard-app

npm i

echo
echo '----- Stopping Images'
echo

docker stop $(docker ps -qa)