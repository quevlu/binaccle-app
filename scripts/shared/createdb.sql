CREATE DATABASE IF NOT EXISTS `binaccle` COLLATE 'utf8_general_ci';
GRANT ALL ON `binaccle`.* TO 'default'@'%';
FLUSH PRIVILEGES;

CREATE DATABASE IF NOT EXISTS `binaccle-testing` COLLATE 'utf8_general_ci';
GRANT ALL ON `binaccle-testing`.* TO 'default'@'%';
FLUSH PRIVILEGES;